package com.zhuoli.service.multi.datasource.transaction.repository.repository.transaction;

public class DbTxConstants {

    public static final String DB1_TX = "masterTransactionManager";

    public static final String DB2_TX = "slaveTransactionManager";
}
