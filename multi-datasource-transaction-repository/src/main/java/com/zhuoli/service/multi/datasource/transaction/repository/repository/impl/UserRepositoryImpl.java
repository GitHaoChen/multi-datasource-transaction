package com.zhuoli.service.multi.datasource.transaction.repository.repository.impl;

import com.zhuoli.service.multi.datasource.trancation.common.enums.DataStatusEnum;
import com.zhuoli.service.multi.datasource.transaction.repository.mapper.master.MasterUserMapper;
import com.zhuoli.service.multi.datasource.transaction.repository.mapper.slave.SlaveUserMapper;
import com.zhuoli.service.multi.datasource.transaction.repository.model.master.MasterUser;
import com.zhuoli.service.multi.datasource.transaction.repository.model.slave.SlaveUser;
import com.zhuoli.service.multi.datasource.transaction.repository.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 10:38
 * @Description:
 */
@Repository
@AllArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private MasterUserMapper masterUserMapper;

    private SlaveUserMapper slaveUserMapper;

    @Override
    public void createUser(String userName, String description) {
        MasterUser masterUser = new MasterUser();

        masterUser.setUserName(userName);
        masterUser.setDescription(description);
        masterUser.setIsDeleted(DataStatusEnum.EXIST.getCode());
        masterUserMapper.insertSelective(masterUser);

        SlaveUser slaveUser = new SlaveUser();
        slaveUser.setUserName(userName);
        slaveUser.setDescription(description);
        slaveUser.setIsDeleted(DataStatusEnum.EXIST.getCode());
        slaveUserMapper.insertSelective(slaveUser);
    }
}
