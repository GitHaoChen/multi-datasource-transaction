package com.zhuoli.service.multi.datasource.transaction.repository.mapper.master;

import com.zhuoli.service.multi.datasource.transaction.repository.model.master.MasterUser;
import com.zhuoli.service.multi.datasource.transaction.repository.model.master.MasterUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface MasterUserMapper {
    long countByExample(MasterUserExample example);

    int deleteByExample(MasterUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MasterUser record);

    int insertSelective(MasterUser record);

    List<MasterUser> selectByExampleWithRowbounds(MasterUserExample example, RowBounds rowBounds);

    List<MasterUser> selectByExample(MasterUserExample example);

    MasterUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MasterUser record, @Param("example") MasterUserExample example);

    int updateByExample(@Param("record") MasterUser record, @Param("example") MasterUserExample example);

    int updateByPrimaryKeySelective(MasterUser record);

    int updateByPrimaryKey(MasterUser record);
}