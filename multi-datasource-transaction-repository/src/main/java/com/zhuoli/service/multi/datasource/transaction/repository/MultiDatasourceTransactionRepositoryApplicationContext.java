package com.zhuoli.service.multi.datasource.transaction.repository;

import com.zhuoli.service.multi.datasource.trancation.common.MultiDatasourceTransactionCommonApplicationContext;
import com.zhuoli.service.multi.datasource.transaction.repository.config.MasterDataSourceConfig;
import com.zhuoli.service.multi.datasource.transaction.repository.config.SlaveDataSourceConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 12:19
 * @Description:
 */
@Configuration
@ComponentScan
@Import(value = {
        MultiDatasourceTransactionCommonApplicationContext.class,
        MasterDataSourceConfig.class,
        SlaveDataSourceConfig.class
})
public class MultiDatasourceTransactionRepositoryApplicationContext {
}
