package com.zhuoli.service.multi.datasource.transaction.repository.repository;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 10:35
 * @Description:
 */
public interface UserRepository {
    void createUser(String userName, String description);
}
