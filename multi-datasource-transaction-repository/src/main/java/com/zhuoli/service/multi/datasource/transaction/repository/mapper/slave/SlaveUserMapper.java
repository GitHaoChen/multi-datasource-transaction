package com.zhuoli.service.multi.datasource.transaction.repository.mapper.slave;

import com.zhuoli.service.multi.datasource.transaction.repository.model.slave.SlaveUser;
import com.zhuoli.service.multi.datasource.transaction.repository.model.slave.SlaveUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface SlaveUserMapper {
    long countByExample(SlaveUserExample example);

    int deleteByExample(SlaveUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SlaveUser record);

    int insertSelective(SlaveUser record);

    List<SlaveUser> selectByExampleWithRowbounds(SlaveUserExample example, RowBounds rowBounds);

    List<SlaveUser> selectByExample(SlaveUserExample example);

    SlaveUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SlaveUser record, @Param("example") SlaveUserExample example);

    int updateByExample(@Param("record") SlaveUser record, @Param("example") SlaveUserExample example);

    int updateByPrimaryKeySelective(SlaveUser record);

    int updateByPrimaryKey(SlaveUser record);
}