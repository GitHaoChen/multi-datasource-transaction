package com.zhuoli.service.multi.datasource.trancation.common;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 11:00
 * @Description:
 */
@Configuration
@ComponentScan
public class MultiDatasourceTransactionCommonApplicationContext {
}
