package com.zhuoli.service.multi.datasource.trancation.common.request;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 11:44
 * @Description:
 */
@Getter
@Setter
public class CreateUserRequest {
    private String name;

    private String description;

}
