package com.zhuoli.service.multi.datasource.trancation.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 10:55
 * @Description:
 */
@AllArgsConstructor
@Getter
public enum DataStatusEnum {
    EXIST(0, "未删除"),
    DELETED(1, "已删除");

    private int code;
    private String desc;

    public static boolean isExist(int code) {
        return EXIST.getCode() == code;
    }

    public static boolean isDeleted(int code) {
        return DELETED.getCode() == code;
    }
}
