package com.zhuoli.service.multi.transaction.core.service;

import com.zhuoli.service.multi.datasource.trancation.common.MultiDatasourceTransactionCommonApplicationContext;
import com.zhuoli.service.multi.datasource.transaction.repository.MultiDatasourceTransactionRepositoryApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 12:22
 * @Description:
 */
@Configuration
@ComponentScan
@Import(value = {
        MultiDatasourceTransactionCommonApplicationContext.class,
        MultiDatasourceTransactionRepositoryApplicationContext.class
})
public class MultiDatasourceTransactionCoreApplicationContext {
}
