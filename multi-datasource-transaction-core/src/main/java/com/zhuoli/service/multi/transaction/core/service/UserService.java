package com.zhuoli.service.multi.transaction.core.service;

import com.zhuoli.service.multi.datasource.trancation.common.request.CreateUserRequest;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 11:20
 * @Description:
 */
public interface UserService {
    void createUser(CreateUserRequest createUserRequest);
}
