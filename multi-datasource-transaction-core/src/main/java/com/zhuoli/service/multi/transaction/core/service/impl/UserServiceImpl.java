package com.zhuoli.service.multi.transaction.core.service.impl;

import com.zhuoli.service.multi.datasource.trancation.common.request.CreateUserRequest;
import com.zhuoli.service.multi.datasource.transaction.repository.aop.MultiTransactional;
import com.zhuoli.service.multi.datasource.transaction.repository.repository.UserRepository;
import com.zhuoli.service.multi.datasource.transaction.repository.repository.transaction.DbTxConstants;
import com.zhuoli.service.multi.transaction.core.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 11:21
 * @Description:
 */
@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    @Override
    @MultiTransactional({DbTxConstants.DB1_TX, DbTxConstants.DB2_TX})
    public void createUser(CreateUserRequest createUserRequest) {
        userRepository.createUser(createUserRequest.getName(), createUserRequest.getDescription());
    }
}
