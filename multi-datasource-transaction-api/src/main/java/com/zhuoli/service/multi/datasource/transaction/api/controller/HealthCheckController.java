package com.zhuoli.service.multi.datasource.transaction.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by gopher on 2017/3/22.
 */
@RestController
@RequestMapping(value = "/health")
public class HealthCheckController {

    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public String check() {
        return "ok";
    }

}
