package com.zhuoli.service.multi.datasource.transaction.api;

import com.zhuoli.service.multi.transaction.core.service.MultiDatasourceTransactionCoreApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.annotation.*;

/**
 * @Author: zhuoli
 * @Date: 2018/7/19 13:41
 * @Description:
 */
@Configuration
@ComponentScan
@EnableAspectJAutoProxy
@SpringBootApplication
@Import(value = {MultiDatasourceTransactionCoreApplicationContext.class})
public class MultiDatasourceTransactionApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(MultiDatasourceTransactionApiApplication.class, args);
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return container -> container.setPort(9898);
    }
}
