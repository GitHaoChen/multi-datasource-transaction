package com.zhuoli.service.multi.datasource.transaction.api.controller;

import com.zhuoli.service.multi.datasource.trancation.common.request.CreateUserRequest;
import com.zhuoli.service.multi.transaction.core.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @Author: zhuoli
 * @Date: 2018/7/22 11:23
 * @Description:
 */
@AllArgsConstructor
@RestController
@RequestMapping(value = "/user")
public class UserController {
    private UserService userService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity createBalanceReminding(@Valid @RequestBody CreateUserRequest createUserRequest) {
        userService.createUser(createUserRequest);
        return ResponseEntity.status(HttpStatus.OK).body("success");
    }
}
